console.log("Hello World!")

// Conditional Statement
// if, else if, else

let numG = -1
if(numG < 0){
    console.log("Hello")
}
if(numG < 0){
    console.log("Hello")
}

// else if

let numH = 1
if(numG > 0){
    console.log("Hello")
}else if(numH > 0){
    console.log("World")
}

//else

if(numG > 0){
    console.log("Hello")
}else if(numH = 0){
    console.log("World")
}else{
    console.log("Again")
}

// if, else if, else statement with a function
let message ="No message."
console.log(message)

function determineTyphoonIntensity(windspeed){
    if (windspeed < 30){
        return "Not  a Typhoon yet."
    } else if (windspeed <= 61){
        return "Tropical Depression Detected."
    } else if(windspeed <= 88){
        return "Tropical Storm Detected."
    } else if (windspeed <= 177){
        return "Severe tropical storm detected"
    } else{
        return "Typhoon detected"
    }

}

message = determineTyphoonIntensity(69)
console.log(message)


message = determineTyphoonIntensity(69)
console.log(message)


message = determineTyphoonIntensity(200)
console.log(message)

message = determineTyphoonIntensity(100)
console.log(message)


message = determineTyphoonIntensity(75)
console.log(message)
if(message == "Tropical Storm Detected."){
    console.warn(message)
}

// Truthy and Falsy

// Truthy Example
if (true){
    console.log("Truthy")
}

if(1){
    console.log("Truthy")
}

if([]){
    console.log('Truthy')
}

// falsy Example
if(false){
    console.log('Falsy')
}
if(0){
    console.log('Falsy')
    
}

if (undefined){
    console.log('Falsy')

}

// Conditional (Ternary) Operator
// Single Statement Execution

let ternaryResult = (1 < -1) ? "Hello" : "World";
console.log("Result of ternary operator: "+ ternaryResult)

// Multiple statement execution
// let name 

// function isOfLegalAge(){
//     name = "John"
//     return "You are of legal the age limit"
// }
// function isUnderAge(){
//     name = "Jane"
//     return "You are under the age limit"
// }
// let age = parseInt(prompt("What is your age? "))
// console.log(age)

// let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge()
// console.log("Result of Ternary Operator in function: "+ legalAge + ", "+ name)

// Switch Statement
let day  = prompt("What day of the week is it today?").toLowerCase()
console.log(day)

switch (day){
    case "monday":
        alert("The color of the day is red");
        break;
    case "tuesday":
        console.log("The color of the day is orange");
        break;
    case "wednesday":
        console.log("The color of the day is yellow");
        break;
    case "thursday":
        console.log("The color of the day is green");
        break;
    case "friday":
        console.log("The color of the day is blue");
        break;
    case "saturday":
        console.log("The color of the day is indigo");
        break;
    case "sunday":
        console.log("The color of the day is violet");
        break;
    default:
        console.log("Please input a valid day.")
}

// Try-Catch-Finally Statement
function showInternsityAlert(windSpeed){
    try{
        // try to attempt to execute a code
        alert(determineTyphoonIntensity(windSpeed))

    }
    catch (error){
        console.log(typeof error)
        console.log(error.message)
    }
    finally{
        alert("Intensity updates will show new alert.")
    }
}
showInternsityAlert(56)